<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 28.08.2017
 * Time: 11:27
 */

namespace Szkolenie;


class Response extends \Symfony\Component\HttpFoundation\Response
{

    /**
     * Response constructor.
     */
    public function __construct($context, $status = 200, $headers = array())
    {
        if(is_array($context)) {
            $context = json_encode($context);
        }
        parent::__construct($context, $status, $headers);
    }
}