<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 28.08.2017
 * Time: 10:13
 */

namespace Szkolenie\Controller;


use Predis\Client;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Szkolenie\Repository\FollowRepository;
use Szkolenie\Repository\MessageRepository;
use Szkolenie\Repository\WallRepository;
use Szkolenie\Response;

class Wall implements ControllerProviderInterface
{
    /** @var MessageRepository */
    private $messageRepo;
    /** @var WallRepository */
    private $wallRepo;

    /**
     * Wall constructor.
     * @param MessageRepository $messageRepo
     * @param WallRepository $wallRepository
     */
    public function __construct(MessageRepository $messageRepo, WallRepository $wallRepository)
    {
        $this->messageRepo = $messageRepo;
        $this->wallRepo = $wallRepository;
    }

    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        $controllers->match('/{id}', function ($id) use ($app) {

            $messagesIds = $this->wallRepo->getByUserId($id);
            if(empty($messagesIds)) {
                return new Response([]);
            }
            $messages = $this->messageRepo->getMany($messagesIds);
            $result = [
                'count' => count($messages),
                'results' => $messages
            ];
            return new \Szkolenie\Response($result);
        });

        return $controllers;
    }

}