<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 28.08.2017
 * Time: 10:13
 */

namespace Szkolenie\Controller;


use PhpAmqpLib\Connection\AMQPStreamConnection;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Silex\Controller;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Szkolenie\DataGenerator;
use Szkolenie\Repository\FollowRepositoryPdo;
use Szkolenie\Repository\MessageRepository;
use Szkolenie\Repository\MessageRepositoryPdo;
use Szkolenie\Repository\UserRepositoryPdo;
use Szkolenie\Response;
use Szkolenie\Tools\RabbitSender;

class Message implements ControllerProviderInterface
{
    /** @var  MessageRepository */
    private $messageRepo;

    public function connect(Application $app)
    {
        $c = $app['controllers_factory'];
        $c->match('/', function (Request $request) use ($app) {
            $repo = new MessageRepositoryPdo($app['pdo']);
            $data = $repo->selectAll();
            return new Response($data);
        });

        $c->match('/add/{userId}/{message}', function (Request $request) use ($app) {

            $message = $request->get('message');
            $userId = $request->get('userId');

            $params = [
                'message' => $message,
                'userId' => $userId
            ];

            RabbitSender::createAndSendMessage('dg_messages_add', $params);

            //$this->messageRepo->add($message, $userId);
            return new Response('ok');
        });

        return $c;
    }

}