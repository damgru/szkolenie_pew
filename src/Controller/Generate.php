<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 28.08.2017
 * Time: 10:13
 */

namespace Szkolenie\Controller;


use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Silex\Controller;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Szkolenie\DataGenerator;
use Szkolenie\Repository\FollowRepositoryPdo;
use Szkolenie\Repository\MessageRepositoryPdo;
use Szkolenie\Repository\UserRepositoryPdo;
use Szkolenie\Response;

class Generate implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->match('/users', function (Request $request) use ($app) {

            $mRepo = new MessageRepositoryPdo($app['pdo']);
            $uRepo = new UserRepositoryPdo($app['pdo']);
            $fRepo = new FollowRepositoryPdo($app['pdo']);
            set_time_limit(0);

            $startId = 11;
            $endId = 425;
            $d = new DataGenerator($startId);
            for($i = $startId;$i<$endId;$i++)
            {
                $user = $d->user();
                $id = $uRepo->add($user['name']);
                $follows = rand(30,50);
                for($f = 0; $f < $follows ; $f++)
                {
                    $fRepo->add($id, rand($startId,$endId));
                }
            }
            return new Response('ok');
        });

        $controllers->match('/messages', function (Request $request) use ($app) {

            $mRepo = new MessageRepositoryPdo($app['pdo']);
            set_time_limit(0);

            $startId = 11;
            $endId = 425;
            $messages = 200;
            $d = new DataGenerator($startId);
            for($i = 0;$i<$messages;$i++)
            {
                $data = [];
                for ($y = 0; $y<10; $y++)
                {
                    $data[] = [
                        'user_id' => rand($startId, $endId),
                        'message' => $d->getLorem(),
                    ];

                }
                $mRepo->addMany($data);
            }
            return new Response('ok');
        });
        return $controllers;
    }

}