<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 28.08.2017
 * Time: 10:13
 */

namespace Szkolenie\Controller;

use Predis\Client;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;

class Redis implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        $controllers->match('/{id}', function ($id) use ($app) {
            /** @var Client $redis */
            $redis = $app['redis'];
            $response = $redis->get($id);
            return new \Szkolenie\Response($response);
        });

        $controllers->match('/{id}/{value}', function ($id, $value) use ($app) {
            /** @var Client $redis */
            $redis = $app['redis'];
            $response = $redis->set($id, $value);
            return new \Szkolenie\Response($response);
        });

        return $controllers;
    }

}