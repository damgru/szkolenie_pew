<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 29.08.2017
 * Time: 12:56
 */

namespace Szkolenie\Repository;

interface WallRepository
{
    public function getByUserId($userId);
}