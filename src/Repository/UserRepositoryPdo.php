<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 28.08.2017
 * Time: 10:54
 */

namespace Szkolenie\Repository;

class UserRepositoryPdo extends DefaultRepositoryPdo implements UserRepository
{
    protected $tableName = 'dg_users';

    public function add($name)
    {
        $index = $this->getOneRow("SELECT nextval('dg_users_id_seq') as index");
        $index = $index['index'];
        $sql = <<<SQL
INSERT INTO dg_users (id, name) VALUES (?, ?)
SQL;
        $this->execute($sql, [$index, $name]);
        return $index;
    }
}