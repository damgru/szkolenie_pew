<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 29.08.2017
 * Time: 10:44
 */

namespace Szkolenie\Repository;

interface FollowRepository
{
    public function add($userId, $followUserId);

    public function getByUserId($userId);
}