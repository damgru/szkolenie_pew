<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 28.08.2017
 * Time: 10:54
 */

namespace Szkolenie\Repository;

class FollowRepositoryPdo extends DefaultRepositoryPdo implements FollowRepository
{
    protected $tableName = 'dg_follow';

    public function add($userId, $followUserId)
    {
        $sql = <<<SQL
INSERT INTO dg_follow (user_id, follow_user_id) VALUES (?, ?)
SQL;
        $this->execute($sql, [$userId, $followUserId]);
    }

    public function getByUserId($userId)
    {
        $sql = <<<SQL
      SELECT f.follow_user_id 
      FROM dg_follow f
      WHERE f.user_id IN (?)
      UNION 
      SELECT ? AS follow_user_id
SQL;
        $rows = $this->getAllRows($sql, [$userId, $userId]);
        return array_column($rows, 'follow_user_id');
    }
}