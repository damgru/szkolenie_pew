<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 29.08.2017
 * Time: 12:53
 */

namespace Szkolenie\Repository;


class WallRepositoryPdo extends DefaultRepositoryPdo implements WallRepository
{

    public function getByUserId($userId)
    {
        $sql = <<<SQL
SELECT message_id FROM dg_wall WHERE user_id = ?
SQL;

        $results = $this->getAllRows($sql, [$userId]);
        return array_column($results, 'message_id');
    }

}