<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 29.08.2017
 * Time: 10:45
 */

namespace Szkolenie\Repository;


use Predis\Client;

class FollowRepositoryCached implements FollowRepository
{
    /** @var FollowRepository */
    private $repo;
    /** @var Client */
    private $redis;
    private $prefix;

    /**
     * FollowRepositoryCached constructor.
     * @param FollowRepository $repo
     * @param Client $redis
     * @param $prefix
     */
    public function __construct(FollowRepository $repo, Client $redis, $prefix = 'dg_follow_')
    {
        $this->repo = $repo;
        $this->redis = $redis;
        $this->prefix = $prefix;
    }

    public function add($userId, $followUserId)
    {
        $this->repo->add($userId, $followUserId);
        $this->redis->del([$this->prefix.$userId]);
    }

    public function getByUserId($userId)
    {
        $name = $this->prefix.$userId;
        $result = json_decode($this->redis->get($name), true);
        if(empty($result)) {
            $result = $this->repo->getByUserId($userId);
            $this->redis->set($name, json_encode($result));
        }
        return $result;
    }

}