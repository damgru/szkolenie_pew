<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 29.08.2017
 * Time: 11:50
 */

namespace Szkolenie\Repository;
use Predis\Client;

class MessageRepositoryCached implements MessageRepository
{
    /** @var MessageRepository */
    private $repo;
    /** @var Client */
    private $redis;

    /**
     * MessageRepositoryCached constructor.
     * @param MessageRepository $repo
     * @param Client $redis
     */
    public function __construct(MessageRepository $repo, Client $redis)
    {
        $this->repo = $repo;
        $this->redis = $redis;
    }


    public function addMany(array $messages)
    {
        return $this->repo->addMany($messages);
    }

    public function add(string $message, int $userId)
    {
        return $this->repo->add($message, $userId);
    }

    public function getMany($ids)
    {
        $ids = array_unique($ids);
        echo 'prosze o dane: '.join(',', $ids).'<br>';
        //POBIERANIE Z REDISA
        $keys = [];
        $gotIds = [];
        foreach ($ids as $id) {
            $keys[] = 'dg_m_'.$id;
        }
        $messages = $this->redis->mget($keys);
        $messages = array_filter($messages);
        foreach ($messages as $message)
        {
            if(!empty($message)) {
                $message = json_decode($message, true);
                $gotIds[] = $message['id'];
            }
        }

        echo 'w cache znalazlem: '.join(',', $gotIds).'<br>';


        //POBIERANIE Z BAZY
//        $missingIds = [];
//        foreach ($ids as $id) {
//            if(!in_array($id, $gotIds)) {
//                $missingIds[] = $id;
//            }
//        }
        $missingIds = array_diff($ids, $gotIds);
        echo 'a tych szukam w bazie: '.join(',', $missingIds).'<br>';
        $messagesFromDb = [];
        if(!empty($missingIds)) {

            $messagesFromDb = $this->repo->getMany($missingIds);
            foreach ($messagesFromDb as $message)
            {
                $this->redis->set('dg_m_'.$message['id'], json_encode($message));
                $resultMessages[] = $message;
            }
        }

        //SCALANIE
        return array_merge($messages, $messagesFromDb);
    }

}