<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 28.08.2017
 * Time: 10:54
 */

namespace Szkolenie\Repository;

class MessageRepositoryPdo extends DefaultRepositoryPdo implements MessageRepository
{
    protected $tableName = 'dg_messages';

    /**
     * @param string $message
     * @param int $userId
     */
    public function add(string $message, int $userId)
    {
        $sql = <<<SQL
          INSERT INTO dg_messages (user_id, message) VALUES (?,?) 
SQL;
        $s = $this->execute($sql, [$userId, $message]);
    }

    /**
     * @param array $messagesIds
     * @return array
     */
    public function getMany($messagesIds)
    {
        $in = join(',', $messagesIds);
        $sql = <<<SQL
          SELECT 
              m.*
            FROM dg_messages m
            WHERE m.id IN ({$in})
SQL;
        return $this->getAllRows($sql);
    }


    /**
     * @param $userId
     * @return array
     */
    public function getMessagesForUserId($userId)
    {
        $sql = <<<SQL
          SELECT 
              m.*
            FROM dg_messages m
            WHERE user_id IN (
              SELECT f.follow_user_id 
              FROM dg_follow f
              WHERE f.user_id IN (?)
              UNION 
              SELECT ? AS follow_user_id
            )
            ORDER BY m.create_date DESC
SQL;
        return $this->getAllRows($sql, [$userId, $userId]);
    }

    /**
     * @param array $messages
     */
    public function addMany(array $messages)
    {
        $values = [];
        $params = [];
        foreach ($messages as $message) {
            $values[] = " ({$message['user_id']}, '{$message['message']}') ";
        }

        $sql = <<<SQL
          INSERT INTO dg_messages (user_id, message)
VALUES 
SQL;
        $sql .= join (", ", $values);
        $s = $this->execute($sql);
    }
}