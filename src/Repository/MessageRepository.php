<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 29.08.2017
 * Time: 11:29
 */

namespace Szkolenie\Repository;

interface MessageRepository
{
    /**
     * @param array $messages
     */
    public function addMany(array $messages);

    /**
     * @param string $message
     * @param int $userId
     * @return
     */
    public function add(string $message, int $userId);

    /**
     * @param array $ids
     * @return array
     */
    public function getMany($ids);
}