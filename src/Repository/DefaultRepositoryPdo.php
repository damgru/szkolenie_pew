<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 28.08.2017
 * Time: 10:54
 */

namespace Szkolenie\Repository;

use PDO;
use PDOStatement;

abstract class DefaultRepositoryPdo
{
    /** @var  \PDO */
    private $pdo;

    /**
     * @var int
     */
    public static $transactionCounter = 0;

    /**
     * @var string
     */
    protected $tableName = '';

    /**
     * DefaultPDORepository constructor.
     * @param $pdo
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @return bool|string
     */
    public function getCurrentDataTime()
    {
        return date("Y-m-d H:i:s");
    }

    /**
     * @return bool|string
     */
    public function getCurrentData()
    {
        return date("Y-m-d");
    }

    /**
     * Zwraca nazwę tablicy, którą operuje dany model
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @return PDO
     */
    protected function getPDO()
    {
        return $this->pdo;
    }

    /**
     * @param $message
     */
    protected function showError($message)
    {
        trigger_error(self::class . ' -> ' . $message, E_USER_ERROR);
    }

    /**
     * @param $params ['nazwa_kolumny'] = wartość_kolumny
     * @return PDOStatement
     */
    public function insert($params, $autoId = true)
    {
        if ($autoId) $params['id'] = null;
        $values = array();
        foreach ($params as $key => $value) {
            $values[] = ':' . $key;
        }

        $sql_names = '`' . implode('`, `', array_keys($params)) . '`';
        $sql_values = implode(', ', $values);

        $sql = "INSERT INTO {$this->tableName} ($sql_names) VALUES ( $sql_values );";
        $q = $this->execute($sql, $params);
        return $q;
    }

    /**
     * @param string|null $name [optional] <p>
     * Name of the sequence object from which the ID should be returned
     * @return string
     */
    public function getLastInsertId($name = null)
    {
        return $this->getPDO()->lastInsertId($name);
    }

    /**
     * @param $params []['nazwa_kolumny'] = wartość_kolumny
     * @return PDOStatement
     */
    public function insertMany($params, $autoId = true)
    {
        if (empty($params)) return null;

        $sql_all_values = array();
        $sql_names = '`' . implode('`, `', array_keys($params[0])) . '`';
        foreach ($params as $record) {
            $values = array();
            if ($autoId) $params['id'] = null;
            foreach ($record as $key => $value) {
                $values[] = ':' . $key;
            }
            $sql_values = implode(', ', $values);
            $sql_all_values[] = $sql_values;
        }

        $sql_all_values = implode(' ), ( ', $sql_all_values);
        $sql = "INSERT INTO {$this->tableName} ($sql_names) VALUES ( $sql_all_values );";
        $q = $this->execute($sql, $params);
        return $q;
    }

    /**
     * @param $wheres
     * @param $values
     * @param $operator string
     * @return array
     */
    public function selectAll($wheres = array(), $values = array(), $operator = 'AND')
    {
        if (!is_array($wheres)) {
            $wheres = array($wheres);
        }
        if (!is_array($values)) {
            $values = array($values);
        }

        $sql = "SELECT * FROM {$this->tableName}";
        if (!empty($wheres)) {
            $sql .= ' WHERE ' . join(" $operator ", $wheres);
        }
        return $this->execute($sql, $values)->fetchAll();
    }

    /**
     * @param $page
     * @param int $rowsPerPage
     * @return array
     */
    public function selectLimit($page, $rowsPerPage = 20)
    {
        if ($page <= 0) $page = 1;
        $sql = "SELECT * FROM {$this->tableName} LIMIT ? OFFSET ?";
        $params = array($rowsPerPage, ($page * $rowsPerPage));
        return $this->execute($sql, $params)->fetchAll();
    }

    /**
     * @param string $groupBy
     * @return array
     */
    public function countGroupBy($groupBy = '')
    {
        $sql_groupby = '';
        if (!empty($groupBy)) {
            if (!is_array($groupBy)) $groupBy = array($groupBy);
            if (count($groupBy) > 0) {
                $sql_groupby = "GROUP BY " . join(',', $groupBy);
            }

        }
        $sql = "SELECT COUNT(*) FROM {$this->tableName} $sql_groupby";
        return $this->execute($sql)->fetchAll();
    }

    /**
     * @return string
     */
    public function count()
    {
        $sql = "SELECT COUNT(*) FROM {$this->tableName}";
        return $this->execute($sql)->fetchColumn();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function selectById($id)
    {
        $sql = "SELECT * FROM {$this->tableName} WHERE id = ?";
        return $this->execute($sql, array($id))->fetch();
    }

    /**
     * @param $id
     * @param $params
     * @return PDOStatement
     */
    public function updateById($id, $params)
    {
        $sets = array();
        foreach ($params as $key => $value) {
            $sets[] = "$key = ?";
        }

        $sql = "UPDATE {$this->tableName} SET " . implode(',', $sets) . " WHERE id = ?";
        $values = array_merge(array_values($params), array($id));
        return $this->execute($sql, $values);
    }

    /**
     * @param $params
     * @param $wheres
     * @return PDOStatement
     */
    public function update($params, $wheres)
    {
        $sets = array();
        foreach ($params as $key => $value) {
            $sets[] = "$key = ?";
        }

        $where = array();
        foreach ($wheres as $key => $value) {
            $where[] = "$key = ?";
        }

        $sql = "UPDATE {$this->tableName} SET " . implode(',', $sets) . " WHERE " . implode(' AND ', $where);
        $values = array_merge(array_values($params), array_values($wheres));
        return $this->execute($sql, $values);
    }

    /**
     * @param $wheres
     * @param $operator string zlaczenia warunkow where
     * @return PDOStatement
     */
    public function delete($wheres, $operator = 'AND')
    {
        $where = array();
        foreach ($wheres as $key => $value) {
            $where[] = "$key = ?";
        }

        $sql = "DELETE FROM {$this->tableName} WHERE " . implode(' ' . $operator . ' ', $where);
        $values = array_values($wheres);
        return $this->execute($sql, $values);
    }

    /**
     * @param $id
     * @return PDOStatement
     */
    public function deleteById($id)
    {
        $sql = "DELETE FROM {$this->tableName} WHERE id = ?";
        return $this->execute($sql, array($id));
    }

    /**
     * @param string|array $column_name
     * @param string $query - searching query
     * @param bool $like - condition == or LIKE %query%
     * @param int $limit maksymalna ilość wyników
     * @return array
     */
    public function search($column_name, $query, $like = true, $limit = -1)
    {
        $condition = '=';
        if ($like) {
            $condition = 'LIKE';
            $query = "%$query%";
        }

        if (!is_array($column_name)) {
            $column_name = array($column_name);
        }

        $whereSql = array();
        $whereValue = array();

        foreach ($column_name as $name) {
            $whereSql[] = '`' . $name . '` ' . $condition . ' ?';
            $whereValue[] = $query;
        }

        $sql = "SELECT * FROM {$this->tableName} WHERE " . $this->arrayToQueryWhere($whereSql, 'OR');

        if ($limit > 0) {
            $sql .= " LIMIT " . $limit;
        }

        return $this->getAllRows($sql, $whereValue);
    }

    /**
     * @param array $array
     * @param string $join
     * @return string
     */
    public function arrayToQueryWhere(array $array, $join = 'AND')
    {
        if (empty($array)) {
            return "true";
        }

        return join(" $join ", $array);
    }

    /**
     * @param array $data
     * @return string
     */
    public function arrayToQueryIn(array $data)
    {
        $countData = count($data);
        return ($countData > 0) ? implode(',', array_fill(0, $countData, '?')) : '-1';
    }

    /**
     * @param $sql
     * @param $params
     * @return PDOStatement
     * @throws \Exception
     */
    public function execute($sql, $params = array())
    {
        $s = $this->pdo->prepare($sql);
        $s->execute($params);
        return $s;
    }

    /**
     * @param $sql
     * @param array $params
     * @return array
     */
    public function getAllRows($sql, $params = array())
    {
        $result = $this->execute($sql, $params)->fetchAll(PDO::FETCH_ASSOC);
        return empty($result) ? array() : $result;
    }

    /**
     * @param $sql
     * @param array $params
     * @return array|mixed
     */
    public function getOneRow($sql, $params = array())
    {
        $result = $this->execute($sql, $params)->fetch(PDO::FETCH_ASSOC);
        return empty($result) ? array() : $result;
    }

    /**
     * @param $sql
     * @param array $params
     * @return null|string
     */
    public function getOneColumn($sql, $params = array())
    {
        $result = $this->execute($sql, $params)->fetchColumn();
        return empty($result) ? null : $result;
    }

    /**
     * Rozpoczyna transakcję
     * @return bool <b>TRUE</b> on success or <b>FALSE</b> on failure.
     */
    public function beginTransaction()
    {
        if (!self::$transactionCounter++) {
            return self::getPDO()->beginTransaction();
        }
        self::getPDO()->exec('SAVEPOINT trans' . self::$transactionCounter);
        return self::$transactionCounter >= 0;
    }

    /**
     * Potwierdza transakcję
     * @return bool <b>TRUE</b> on success or <b>FALSE</b> on failure.
     */
    public function commit()
    {
        if (!--self::$transactionCounter) {
            return self::getPDO()->commit();
        }
        return self::$transactionCounter >= 0;
    }

    /**
     * Wycofuje transakcję
     * @return bool <b>TRUE</b> on success or <b>FALSE</b> on failure.
     */
    public function rollback()
    {
        if (--self::$transactionCounter) {
            self::getPDO()->exec('ROLLBACK TO trans' . (self::$transactionCounter + 1));
            return true;
        }
        return self::getPDO()->rollBack();
    }
}