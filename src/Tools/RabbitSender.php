<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 12.07.2017
 * Time: 14:27
 */

namespace Szkolenie\Tools;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitSender
{
    public static function sendMessage($messageBody, $exchange = 'dg-messages')
    {
        $connection = new AMQPStreamConnection(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER, RABBITMQ_PASS, RABBITMQ_VHOST);

        $channel = $connection->channel();
        $channel->exchange_declare($exchange, 'direct');
        $channel->queue_declare($exchange, false, false, false, false);
        $channel->queue_bind($exchange, $exchange);

        $message = new AMQPMessage($messageBody, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
        $channel->basic_publish($message, $exchange);

        $channel->close();
        $connection->close();
    }

    public static function createAndSendMessage($messageName, array $data, $exchange = 'dg-messages')
    {
        self::sendMessage(self::createMessage($messageName, $data), $exchange);
    }

    public static function createMessage($messageName, array $data)
    {
        $response = [
            'name' => $messageName,
            'created' => date("Y-m-d H:i:s"),
            'version' => 1,
            'data' => $data
        ];
        return json_encode($response);
    }

    public static function createEventNameFromClassName($className)
    {
        return str_replace('\\', '_', $className);
    }
}