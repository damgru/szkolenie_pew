<?php

use Phinx\Migration\AbstractMigration;

class InitProject extends AbstractMigration
{
    public function up()
    {
        $sql = <<<SQL
create table dg_users
(
	id serial not null constraint dg_users_pkey	primary key,
	name varchar(40)
);
comment on table dg_users is 'Damian Grudzien - users';

create table dg_follow
(
	user_id serial not null,
	follow_user_id integer
);

create index dg_follow_user_follow_id on dg_follow (user_id, follow_user_id);
comment on table dg_follow is 'Damian Grudzien - follow';

create table dg_messages
(
	id integer not null,
	user_id integer not null,
	message varchar(1000),
	create_date timestamp default now()
)
;

create unique index dg_messages_id_uindex
	on dg_messages (id)
;

create index dg_messages_user_id
	on dg_messages (user_id)
;

create function dg_wall_insert_trigger() returns trigger
	language plpgsql
as $$
BEGIN
  
  INSERT INTO dg_wall SELECT f.user_id, NEW.id as message_id 
                      from dg_follow f WHERE f.user_id = NEW.user_id;
  
  INSERT INTO dg_wall VALUES (NEW.user_id, NEW.id);
  
  IF ( NEW.user_id::INTEGER % 4 = 0 ) THEN
    INSERT INTO dg_wall_0 VALUES (NEW.user_id, NEW.id);
  ELSIF ( NEW.user_id::INTEGER % 4 = 1 ) THEN
    INSERT INTO dg_wall_1 VALUES (NEW.user_id, NEW.id);
  ELSIF ( NEW.user_id::INTEGER % 4 = 2 ) THEN
    INSERT INTO dg_wall_2 VALUES (NEW.user_id, NEW.id);
  ELSIF ( NEW.user_id::INTEGER % 4 = 3 ) THEN
    INSERT INTO dg_wall_3 VALUES (NEW.user_id, NEW.id);
  ELSE
  RAISE EXCEPTION 'Date out of range.  Fix the measurement_insert_trigger() function!';
END IF;
RETURN NULL;
END;
$$
;

create trigger dg_messages_after_insert_trigger
	after insert
	on dg_messages
	for each row
	execute procedure dg_wall_insert_trigger()
;

comment on table dg_messages is 'Damian Grudzien - messages'
;

create table dg_wall_0 ( constraint dg_wall_0_user_id_check check ((user_id % 4) = 0)) inherits (dg_wall);
create table dg_wall_1 ( constraint dg_wall_0_user_id_check check ((user_id % 4) = 1)) inherits (dg_wall);
create table dg_wall_2 ( constraint dg_wall_0_user_id_check check ((user_id % 4) = 2)) inherits (dg_wall);
create table dg_wall_3 ( constraint dg_wall_0_user_id_check check ((user_id % 4) = 3)) inherits (dg_wall);

SQL;
        $this->execute($sql);
    }
}
