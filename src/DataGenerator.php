<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 28.08.2017
 * Time: 12:49
 */

namespace Szkolenie;


class DataGenerator
{
    public $counter = 10;
    public $mCounter = 20;
    public $lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam fermentum aliquet vestibulum.';

    /**
     * DataGenerator constructor.
     * @param int $counter
     */
    public function __construct($counter = 10)
    {
        $this->counter = $counter;
        $this->lorem = explode(' ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam fermentum aliquet vestibulum. Mauris tempor nulla et nisl eleifend, id vehicula ipsum faucibus. Suspendisse ac interdum nisl. Nam dapibus interdum vehicula. Aliquam vestibulum velit sed tellus vehicula, vitae vestibulum sapien pharetra. Donec hendrerit vestibulum orci, et vestibulum erat interdum ac. Maecenas rhoncus consequat erat, ac dictum risus porta sit amet. Proin vel pharetra velit, vel maximus odio. Aliquam auctor mauris eget justo posuere, vel egestas urna tincidunt. Fusce tristique est aliquam, facilisis libero sit amet, euismod libero. Pellentesque dictum tincidunt rhoncus. Donec eget tellus iaculis, iaculis metus non, rhoncus velit. Curabitur eu nisl massa. Phasellus tincidunt accumsan ullamcorper. Vivamus ullamcorper lectus vitae dignissim mollis. Donec sapien lectus, sagittis sit amet nunc nec, condimentum viverra nunc. Fusce feugiat turpis orci, sed vulputate neque euismod pellentesque. Etiam nec nisi ac tortor convallis sagittis nec non nibh. Vestibulum finibus diam ornare, finibus lectus a, aliquam massa. Nullam mattis tellus non risus luctus, vel interdum felis auctor. Quisque mattis pulvinar purus, hendrerit interdum odio elementum vel.');
    }

    public function user()
    {
        $this->counter++;

        return [
            'id' => $this->counter,
            'name' => 'user'.$this->counter,
        ];
    }

    public function message($userId)
    {
        return [
            'id' => $this->mCounter++,
            'user_id' => $userId,
            'message' => $this->getLorem(),
        ];
    }

    public function getLorem()
    {
        $count = rand(5,10);
        $text = '';
        for($i = 0; $i < $count ; $i++)
        {
            $text .= $this->lorem[rand(0, count($this->lorem)-1)].' ';
        }

        return $text;
    }
}