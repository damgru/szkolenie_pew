<?php
require_once 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require_once 'config.php';

$pdo = new PDO("pgsql:dbname=".DB_NAME.";host=".DB_HOST, DB_USER, DB_PASS);

$config = array(
    'environments' => array(
        'default_migration_table' => 'dg_phinxlog',
        'default_database' => 'development',
        'development' => array(
            'name' => 'devdb',
            'connection' => $pdo
        )
    ),
    'paths' => array(
        'migrations' => "%%PHINX_CONFIG_DIR%%/src/Migrations",
        'seeds' => "%%PHINX_CONFIG_DIR%%/src/Migrations/seeds"
    )
);

return $config;