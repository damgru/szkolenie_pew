<?php
define('DB_HOST', '52.212.177.7');
define('DB_USER', 'workshop');
define('DB_PASS', 'workshop');
define('DB_NAME', 'workshop');

define('REDIS_URI', 'tcp://52.212.177.7:6379');

define('RABBITMQ_HOST', '52.212.177.7');
define('RABBITMQ_PORT', 5672);
define('RABBITMQ_USER', 'test');
define('RABBITMQ_PASS', 'test');
define('RABBITMQ_VHOST', '/');
define('RABBITMQ_QUEUE', 'dg-messages');

