localStorage.setItem('user', 1);

function ajaxWallForUser(user) {

    var link = 'http://localhost:8080/wall/' + localStorage.getItem('user');

    $.ajax(
        {
            url: link,
            success: function (result) {
                parseMessages(JSON.parse(result).results);
            },
            error: function (error) {
                console.log(error.statusText);
            }
        }
    );
};

function parseMessages(results) {
    $.each(results, function (i, el) {
        if ($('#'+i).html() != undefined) return;
        var html = '<div id="'+i+'" class="card card-body animated fadeInUp">' +
            '<h4 class="card-title">User id: '+ el.user_id +'</h4>' +
            '<p class="card-text">'+el.message+'</p>' +
            '<div class="flex-row">' +
            '<a class="card-link">'+el.create_date+'</a>' +
            '</div>' +
            '</div>';
        $('#wall').prepend(html);
    });

}
ajaxWallForUser(3);

setInterval(function() {
    ajaxWallForUser(3)
}, 1000);

function setUser(id) {
    $('#wall').html('');
    localStorage.setItem('user', id);
}