<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use Silex\Application;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
$app = new Application();

// silex config
$app['debug'] = true;

// postgres
$app['pdo'] = new PDO("pgsql:dbname=".DB_NAME.";host=".DB_HOST, DB_USER, DB_PASS);
$app['pdo']->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

// redis
$app['redis'] = new \Predis\Client(REDIS_URI);
$app['redis_pool'] = new \Cache\Adapter\Predis\PredisCachePool($app['redis']);

// repos
$app['repo_messages'] = function (Application $app) {
    return new \Szkolenie\Repository\MessageRepositoryCached(
        new \Szkolenie\Repository\MessageRepositoryPdo($app['pdo']),
        $app['redis']
    );
};
$app['repo_wall'] = function (Application $app) {
    return new \Szkolenie\Repository\WallRepositoryPdo($app['pdo']);
};

// routing
$app->mount('/generate', new \Szkolenie\Controller\Generate());
$app->mount('/messages', new \Szkolenie\Controller\Message());
$app->mount('/follow', new \Szkolenie\Controller\Follow());
$app->mount('/wall', new \Szkolenie\Controller\Wall(
    $app['repo_messages'], $app['repo_wall']
));
$app->mount('/redis', new \Szkolenie\Controller\Redis());
$app->run();

/*
new \Szkolenie\Repository\FollowRepositoryCached(
    new \Szkolenie\Repository\FollowRepositoryPdo($app['pdo']),
    $app['redis']
),*/